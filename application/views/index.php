<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Recorder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
</head>
<body>
<main>
<h2>Records</h2>
<button class="button">Add Record</button>
<hr/>
<table>
    <thead>
        <tr>
            <th>Action</th>
            <th>Title</th>
            <th>Thumbnail</th>
            <th>Filename</th>
            <th>Date added</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
</main>

<div class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <h1>New Record</h1>
  <div class="form">
  <form action="/action_page.php">
    <label for="title">Title</label>
    <input type="text" name="title" id="title" placeholder="Enter title..."><br>
    <label for="image">Image</label>
    <input type="file" name="image" id="image"/> 
  </form>
  <button id="" class="add">Add</button>
  </div>

  </div>
</div>

<script
  src="http://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="js/script.js"></script>
</body>
</html>