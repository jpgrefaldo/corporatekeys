<?php defined('SYSPATH') or die('No direct script access.');

class Controller_table extends Controller {

	private $id;

	public function action_index()
	{
		$this->response->body(View::factory('index'));
	}

	public function action_api()
	{
		$this->setId();
		switch ($_SERVER['REQUEST_METHOD']) {
			case 'POST':
				$this->store();
				break;
			case 'DELETE':
				$this->destroy();
				break;			
			default:
				$this->get();
				break;
		}

	}

	public function store()
	{	
		if (isset($_FILES['image']))
		{
			$filename = $this->_save_image($_FILES['image']);
			$_POST['thumbnail'] = 'images/'.$filename;
			$_POST['filename'] = $filename;
		}
		
		$table = ORM::factory('Table',$this->id);
		$table->values($_POST);
		$table->save();	
	}

	public function destroy()
	{
		ORM::factory('Table', $this->id)->delete();
	}

	public function get()
	{
		$data = ORM::factory('Table')->find_all();
		foreach ($data as $object)
		{
			$results_array[] = $object->as_array();
		}
		
		echo json_encode($results_array);	
	}

	public function setId()
	{
		$this->id = $this->request->param('id');
	}

	protected function _save_image($image)
	{
		if (
			! Upload::valid($image) OR
			! Upload::not_empty($image) OR
			! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
		{
			return FALSE;
		}
		
		$directory = DOCROOT.'images/';
		
		if ($file = Upload::save($image, NULL, $directory))
		{
			$filename = strtolower(Text::random('alnum', 20)).'.jpg';
			
			Image::factory($file)
				->resize(200, 200, Image::AUTO)
				->save($directory.$filename);

			// Delete the temporary file
			unlink($file);
			
			return $filename;
		}
		
		return FALSE;
	}
} 
