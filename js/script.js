$(document).ready(function(){
    getData();
    register();
    function register(){
    $('.button, .close, .delete, .edit, .add').off();
    $('.button, .close, .delete, .edit, .add').on('click', function(){
        switch (this.className) {
            case 'button':
                $('.add').attr('id','');
                $('.modal').css({'display':'block'});
                break;
            case 'delete':
                deleteData(this.id);
                break;
            case 'edit':
                popModal(this);
                $('.modal').css({'display':'block'});
                break;
            case 'add':
                postData(this.id);
                $('.modal').css({'display':'none'});
                break;
            default:
                $('.add').attr('id','');
                $('.modal').css({'display':'none'});
                break;
        }
    });
    }

    function popModal(data){
        $('.add').attr('id',data.id);
        title = data.parentElement.nextElementSibling.innerText;
        $('input[name=title]').val(title);
    }

    function postData(id=''){
        
        form = new FormData();
        form.append('image', $('input[type=file]')[0].files[0]);
        form.append('title', $('input[name=title]').val());
        
        requestParser('POST', id, form);
        $('form')[0].reset();
    }

    function deleteData(id){
        requestParser("DELETE",id);
    }

    function getData(){
        requestParser("GET");
        $('tbody').empty();
        $('form')[0].reset();
    }

    function appendData(data){
        $('tbody').empty();
        data.forEach(el => {
            $('tbody')
            .append(
            '<tr>'+
                '<td><button class="delete" id="'+el.id+'">Delete</button> \
                <button class="edit" id="'+el.id+'">Edit</button></td>'+
                '<td>'+el.title+'</td>'+
                '<td><img src="'+el.thumbnail+'"/></td>'+
                '<td>'+el.filename+'</td>'+
                '<td>'+el.date+'</td>'+
            '</tr>'
        )
        });
        register();
    }

    function requestParser(type, id, payload=''){
        $.ajax({
            method:type,
            url:'table/api/'+id,
            data: payload,
            processData: false, 
            contentType: false,
            success: function(respond){
               if(type == 'GET'){
                   appendData(JSON.parse(respond));
               }else{
                   getData();
               }
            }
        })
    }
});
